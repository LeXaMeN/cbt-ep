import 'jquery'; // bug fix https://github.com/webpack-contrib/expose-loader/issues/77#issuecomment-464886116
import './ArrayExtension';
import './MathExtension';
import './StringExtension';
import './NumberExtension';
