import Vue from 'vue';
import VueRouter from 'vue-router';
import Meta from 'vue-meta';
import {Plugin as Vue2Storage} from 'vue2-storage';
import VueSwal from '$api/extensions/VueSwal';
// import VueMoment from '$api/extensions/VueMoment';
import VueUser from '$api/extensions/VueUser';
import VueValidate from '$api/extensions/VueValidate';
import VueRedirect from '$api/extensions/VueRedirect';
import VueMemorizer from '$api/extensions/VueMemorizer';
// import VueI18n from '$api/extensions/VueI18n';

import '$src/extensions/AllExtensions';

import App from './app.vue';
import Router from './pages/router';

// https://router.vuejs.org/
Vue.use(VueRouter);
// https://github.com/declandewet/vue-meta
Vue.use(Meta);

// https://yarkovaleksei.github.io/vue2-storage/ru/
Vue.use(Vue2Storage, {
    prefix: 'app_',
    driver: 'local',
    ttl: 24 * 60 * 60 * 1000 // 24 hours
});

// extensions
Vue.use(VueSwal);
Vue.use(VueValidate);
// Vue.use(VueMoment);
Vue.use(VueUser);
Vue.use(VueRedirect);
Vue.use(VueMemorizer);

// http://kazupon.github.io/vue-i18n/started.html
// Vue.use(VueI18n, {
//     locale: navigator.language?.includes('ru') ? 'ru' : 'en',
//     // https://medium.com/front-end-weekly/webpack-and-dynamic-imports-doing-it-right-72549ff49234
//     // messages: {
//     //     en: import(/* webpackIgnore: true */ '/lib/i18n/en.js'),
//     //     ru: import(/* webpackIgnore: true */ '/lib/i18n/ru.js')
//     // }
//     // silentTranslationWarn: process.env.Mode === 'production',
//     // silentFallbackWarn: process.env.Mode === 'production'
// });

/**
 * Create a fresh App Application instance
 */
new Vue({
    el: '#app',
    router: Router,
    // i18n: window.i18n,
    render: h => h(App), //extends: App,
    beforeCreate() {
        window.App = this;
    }
});
