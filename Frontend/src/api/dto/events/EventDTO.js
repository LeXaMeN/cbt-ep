export default class EventDTO {
    /**
     * @param {number} membersLimit
     * @param {string} name
     * @param {string} description
     * @param {string} time
     * @param {string} location
     */
    constructor(membersLimit, name, description, time, location) {
        this.membersLimit = membersLimit;
        this.name = name;
        this.description = description;
        this.time = time;
        this.location = location;
    }

    /**
     * @type number
     */
    membersLimit;
    /**
     * @type string
     */
    name;
    /**
     * @type string
     */
    description;
    /**
     * @type string
     */
    time;
    /**
     * @type string
     */
    location;
}