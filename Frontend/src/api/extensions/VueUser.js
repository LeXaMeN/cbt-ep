import UserController from '$api/controllers/UserController';
import UserTokenController from '$api/controllers/UserTokenController';
import User from '$api/entity/User';

export default class VueUser {
    static install = function (Vue, options) {
        Vue.prototype.$user = new Vue({
            data: () => new User({
                // id: 0,
                // login: 'Anonymous',
                // roles: []
            }),
            computed: {
                token: {
                    get() {
                        return this.$storage.get('token', null);
                    },
                    set(value) {
                        if (value) {
                            this.$storage.set('token', value, {ttl: parseInt(process.env.Api.Token.Lifetime)});
                        } else {
                            this.$storage.remove('token');
                        }
                    }
                },
                loggedIn() {
                    return this.$storage.has('token');
                },
                isAdmin() {
                    return this.roles?.includes('admin');
                },
                isPartner() {
                    return this.roles?.includes('partner');
                }
            },
            created() {
                if (this.loggedIn) {
                    UserController.Get(user => {
                        Object.keys(user).forEach(key => {
                            this[key] = user[key];
                        });
                    });
                }
            },
            methods: {
                loginRequired() {
                    if (!this.loggedIn) {
                        Vue.prototype.$redirect.toLoginPage();
                    }
                },
                logout() {
                    UserTokenController.Delete(this.token,
                        (result) => {
                            this.token = null;
                            window.location = '/';
                        }
                    );
                }
            }
        });
    }
}
