﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Backend.Extensions.Database;
using Backend.Utils.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Data.Repositories
{
    public class UserTokenRepository
    {
        private string GetRandomValue(UserTokenType type)
        {
            switch (type)
            {
                case UserTokenType.Registration:
                    return new Random().Next(100_000_000, 1_000_000_000).ToString(); // 9 digits
                case UserTokenType.RefreshToken:
                case UserTokenType.PasswordReset:
                    return Hash.Random(Hash.Algorithm.SHA512);
                default:
                    throw new ArgumentOutOfRangeException(nameof(UserTokenType), type, null);
            }
        }

        public UserToken Create(DatabaseContext db, int userId, UserTokenType type, string ipAddress, DateTime expiresAt)
        {
            var token = new UserToken() {
                UserId = userId,
                Type = type,
                Value = GetRandomValue(type),
                IpAddress = ipAddress,
                ExpiresAt = expiresAt
            };

            db.UserTokens.Add(token);
            db.SaveChanges();

            return token;
        }

        public UserToken Update(DatabaseContext db, int userId, UserTokenType type, string value, string ipAddress, DateTime expiresAt)
        {
            var entity = db.Database.Transaction(() =>
            {
                var token = Read(db, userId, type, value);
                token.Value = GetRandomValue(type);
                token.IpAddress = ipAddress;
                token.ExpiresAt = expiresAt;
                db.SaveChanges();
                return token;
            });

            return entity ?? throw new HttpNotFoundException(nameof(UserToken), value);
        }

        public bool ExistAny(DatabaseContext db, int userId, UserTokenType type, bool checkExpires = false)
        {
            return checkExpires
                ? db.UserTokens.Any(t => t.UserId == userId && t.Type == type)
                : db.UserTokens.Any(t => t.UserId == userId && t.Type == type && t.ExpiresAt >= DateTime.UtcNow);
        }

        private UserToken Read(DatabaseContext db, int userId, UserTokenType type, string value)
        {
            return db.UserTokens
                .Where(t => t.UserId == userId && t.Type == type && t.Value == value && t.ExpiresAt >= DateTime.UtcNow)
                .OrderByDescending(t => t.Id).FirstOrDefault() ?? throw new HttpNotFoundException(nameof(UserToken), value);
        }

        public UserToken ReadonlyFirstOrNull(DatabaseContext db, int userId, UserTokenType type, bool checkExpires = false)
        {
            var query = db.UserTokens.AsNoTracking()
                .Where(t => t.UserId == userId && t.Type == type);

            if (checkExpires == false)
                query = query.Where(t => t.ExpiresAt >= DateTime.UtcNow);

            return query.OrderByDescending(t => t.Id).FirstOrDefault();
        }

        public UserToken ReadonlyOrNull(DatabaseContext db, int userId, UserTokenType type, string value, bool checkExpires = false)
        {
            var query = db.UserTokens.AsNoTracking()
                .Where(t => t.UserId == userId && t.Type == type && t.Value == value);

            if (checkExpires == false)
                query = query.Where(t => t.ExpiresAt >= DateTime.UtcNow);

            return query.OrderByDescending(t => t.Id).FirstOrDefault();
        }

        public bool Delete(DatabaseContext db, int userId, UserTokenType type, string value)
        {
            return db.UserTokens.Where(t => t.UserId == userId && t.Type == type && t.ExpiresAt >= DateTime.UtcNow && t.Value == value).Delete() > 0;
        }
        public int DeleteAllExcept(DatabaseContext db, int userId, UserTokenType type, string value)
        {
            return db.UserTokens.Where(t => t.UserId == userId && t.Type == type && t.ExpiresAt >= DateTime.UtcNow && t.Value != value).Delete();
        }
        public int DeleteAll(DatabaseContext db, int userId, UserTokenType type)
        {
            return db.UserTokens.Where(t => t.UserId == userId && t.Type == type && t.ExpiresAt >= DateTime.UtcNow).Delete();
        }


        public int DeleteAllExpired(DatabaseContext db)
        {
            return db.UserTokens.Where(t => t.ExpiresAt <= DateTime.UtcNow).Delete();
        }
    }
}
