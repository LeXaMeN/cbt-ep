import {Api} from '$api/Api';
import UserProfileDTO from '$api/dto/users/UserProfileDTO';
import UserProfile from '$api/entity/UserProfile';

export default class UserProfileController {
    /**
     * @param {UserProfileDTO} userProfileDTO
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Put(userProfileDTO, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'put',
            url: '/user-profiles',
            data: userProfileDTO,
        }, (object) => thenFunction(Api.ObjectToEntity(object, UserProfile)), catchFunction, finallyFunction);
    }

    /**
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Get(thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'get',
            url: '/user-profiles',
        }, (object) => thenFunction(Api.ObjectToEntity(object, UserProfile)), catchFunction, finallyFunction);
    }
}