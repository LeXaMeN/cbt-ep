﻿using Backend.Extensions.Database;
using Microsoft.EntityFrameworkCore;
using System;

namespace Backend.Extensions.StartupTask.Tasks
{
    public class MigrationStartupTask : IStartupTask
    {
        private IServiceProvider ServiceProvider;

        public MigrationStartupTask(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public void Execute()
        {
            using (var scope = ServiceProvider.CreateScope(out var db))
            {
                // CREATE SCHEMA `ep` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
                //db.Database.EnsureDeleted();
                //db.Database.EnsureCreated();
                db.Database.Migrate();
            }
        }
    }
}
