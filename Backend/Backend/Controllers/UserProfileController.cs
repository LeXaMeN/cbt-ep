﻿using Backend.Data.Context;
using Backend.Data.Repositories;
using Backend.DTO.Users;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [Route("user-profiles")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private UserProfileRepository UserProfileRepository { get; }

        public UserProfileController(DatabaseContext database, UserProfileRepository userProfileRepository)
        {
            Database = database;
            UserProfileRepository = userProfileRepository;
        }

        [HttpPut]
        public IActionResult Put([FromBody] UserProfileDTO dto)
        {
            try
            {
                var profile = UserProfileRepository.CreateOrUpdate(Database, User.GetId(), dto.Name, dto.Surname, dto.Age);
                return Ok(profile);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Put), new { UserId = User.GetId() }, dto);
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var profile = UserProfileRepository.ReadonlyOrDefault(Database, User.GetId());
                return Ok(profile);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Get), new { UserId = User.GetId() });
            }
        }
    }
}
