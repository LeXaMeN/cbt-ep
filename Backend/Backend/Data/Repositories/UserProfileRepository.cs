﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Extensions.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data.Repositories
{
    public class UserProfileRepository
    {
        private static readonly object Lock = new object();

        public UserProfile CreateOrUpdate(DatabaseContext db, int userId, string name, string surname, int age)
        {
            var entity = db.Database.Transaction(() =>
            {
                var profile = ReadOrDefault(db, userId);

                if (profile != null)
                {
                    profile.Name = name;
                    profile.Surname = surname;
                    profile.Age = age;
                    db.SaveChanges();
                }

                return profile;
            });

            if (entity == null)
            {
                lock (Lock)
                {
                    entity = ReadonlyOrDefault(db, userId);

                    if (entity == null)
                    {
                        entity = new UserProfile() {
                            UserId = userId,
                            Name = name,
                            Surname = surname,
                            Age = age
                        };
                        db.UserProfiles.Add(entity);
                    }
                    db.SaveChanges();
                }
            }

            return entity;
        }

        private UserProfile ReadOrDefault(DatabaseContext db, int userId)
        {
            return db.UserProfiles.Where(p => p.UserId == userId).OrderBy(p => p.UserId).FirstOrDefault();
        }

        public UserProfile ReadonlyOrDefault(DatabaseContext db, int userId)
        {
            return db.UserProfiles.AsNoTracking().Where(p => p.UserId == userId).OrderBy(p => p.UserId).FirstOrDefault();
        }
    }
}
