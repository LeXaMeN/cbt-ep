import VueI18n from 'vue-i18n';

// https://kazupon.github.io/vue-i18n/guide/formatting.html#named-formatting
export default class {
    static install = function (Vue, options) {
        /**
         * @param value Locale
         * @return {string} Locale
         */
        VueI18n.prototype.setLocale = function (value) {
            this.locale = value;
            if (typeof Vue.prototype.$moment === 'function') {
                Vue.prototype.$moment.locale(value);
            }
            // axios.defaults.headers.common['Accept-Language'] = value;
            document.querySelector('html').setAttribute('lang', value);
            Vue.prototype.$storage.set('locale', value, {ttl: 365 * 24 * 60 * 60 * 1000}); // 1 year
            return value
        };
        VueI18n.prototype.getChoiceIndexDefault = VueI18n.prototype.getChoiceIndex;
        /**
         * @param choice {number} a choice index given by the input to $tc: `$tc('path.to.rule', choiceIndex)`
         * @param choicesLength {number} an overall amount of available choices
         * @returns {number} a final choice index to select plural word by
         **/
        VueI18n.prototype.getChoiceIndex = function (choice, choicesLength) {
            switch (this.locale) {
                default:
                    return this.getChoiceIndexDefault(choice, choicesLength);
                case 'ru':
                    choice %= 100;
                    if (choice >= 9 && choice <= 14) {
                        return 0;
                    }
                    choice %= 10;
                    if (choice === 1) {
                        return 1;
                    }
                    if (choice >= 2 && choice <= 4) {
                        return 2;
                    }
                    return (choicesLength < 4) ? 0 : 3;
            }
        };

        Vue.use(VueI18n);
        window.i18n = new VueI18n(options);

        if (options.locale) {
            window.i18n.setLocale(Vue.prototype.$storage.get('locale', options.locale));
        }
    }
}
