export default class VueMemorizer {
    static install = function (Vue, options) {
        let $storage = Vue.prototype.$storage;
        // remove old values
        // https://developer.mozilla.org/ru/docs/Web/API/Storage
        // https://yarkovaleksei.github.io/vue2-storage/ru/api.html#keys
        $storage.keys().forEach(key => {
            if (key.startsWith($storage.prefix || '')) {
                $storage.fromJSON(key);
            }
        });
        // set memorizer
        Vue.prototype.$memorizer = {
            get: function (key, fallback = null) {
                return $storage.get(key + '$' + window.App.$route.name, fallback);
            },
            take: function (key, fallback = null) {
                let valueKey = key + '$' + window.App.$route.name;
                // update value ttl
                if ($storage.has(valueKey)) {
                    let value = $storage.get(valueKey);
                    $storage.set(valueKey, value);
                    return value;
                }
                return $storage.get(valueKey, fallback);
            },
            has: function (key) {
                return $storage.has(key + '$' + window.App.$route.name);
            },
            set: function (key, data, options = {}) {
                $storage.set(key + '$' + window.App.$route.name, data, options);
                // return data;
            }
        }
    }
}
