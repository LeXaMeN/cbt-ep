﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Providers
{
    public class EmailProvider
    {
        private readonly string Host;
        private readonly int Port;
        private readonly bool Ssl;
        private readonly MailAddress From;
        private readonly NetworkCredential Credentials;

        public EmailProvider(IConfiguration configuration)
        {
            Host = configuration["Email:Host"];
            Port = int.Parse(configuration["Email:Port"]);
            Ssl = bool.Parse(configuration["Email:SSL"]);
            From = new MailAddress(configuration["Email:Sender"]);
            Credentials = new NetworkCredential(configuration["Email:Login"], configuration["Email:Key"]);
        }

        public void Send(string subject, string body, string toEmail, bool isHtml = false)
        {
            using (var message = new MailMessage(From, new MailAddress(toEmail)) {
                Subject = subject,
                SubjectEncoding = Encoding.UTF8,
                Body = isHtml ? $"<!DOCTYPE HTML><html><head><meta charset=\"UTF-8\"></head><body>{body}</body></html>" : body,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = isHtml,
            })
            {
                // https://www.accelo.com/resources/blog/how-to-find-an-email-s-message-id/
                message.Headers.Add("Message-ID", $"<{Guid.NewGuid().ToString()}@{Host}>");
                using (var smtp = new SmtpClient(Host, Port) { EnableSsl = Ssl, Credentials = Credentials })
                {
                    smtp.Send(message);
                }
            }
        }
    }
}
