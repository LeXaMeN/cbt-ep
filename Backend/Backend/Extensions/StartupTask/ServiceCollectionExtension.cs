﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Extensions.StartupTask
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddStartupTask<T>(this IServiceCollection services) where T : class, IStartupTask
        {
            return services.AddTransient<IStartupTask, T>();
        }
    }
}
