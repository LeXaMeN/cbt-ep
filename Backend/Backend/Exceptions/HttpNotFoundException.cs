﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    public class HttpNotFoundException : Exception, IHttpException
    {
        public int StatusCode => (int)HttpStatusCode.NotFound;
        public string DefaultMessage => "Object not found.";

        public HttpNotFoundException(string message) : base(message) { }
        public HttpNotFoundException(string message, Exception innerException) : base(message, innerException) { }

        public HttpNotFoundException(string entityName, object entityKey, Exception innerException = null)
            : base($"{entityName} #{entityKey ?? "NULL"} not found.", innerException) { }
    }
}
