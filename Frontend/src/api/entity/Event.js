import User from '$api/entity/User';

export default class Event {
    constructor(entity = null) {
        if (entity) {
            let value = Object.assign(new Event(), entity);
            if (value.user) {
                value.user = new User(value.user);
            }
            return value;
        }
    }

    /**
     * @type number
     */
    id;
    /**
     * @type number
     */
    userId;
    /**
     * @type ?User
     */
    user;
    /**
     * @type number
     */
    membersLimit;
    /**
     * @type string
     */
    name;
    /**
     * @type string
     */
    description;
    /**
     * @type string
     */
    time;
    /**
     * @type string
     */
    location;
}
