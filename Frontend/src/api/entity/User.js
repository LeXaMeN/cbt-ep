export default class User {
    constructor(entity = null) {
        if (entity) {
            return Object.assign(new User(), entity);
        }
    }

    /**
     * @type number
     */
    id;
    /**
     * @type string
     */
    login;
}
