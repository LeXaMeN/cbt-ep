﻿using Backend.Data.Context;
using Backend.DTO.UserTokens;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Backend.Providers;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("user-tokens")]
    [ApiController]
    public class UserTokenController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private UserTokenService UserTokenService { get; }

        public UserTokenController(DatabaseContext db, UserTokenService userTokenService)
        {
            Database = db;
            UserTokenService = userTokenService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserTokenDTO dto)
        {
            try
            {
                return Ok(UserTokenService.Create(Database, dto, HttpContext.Connection.RemoteIpAddress.ToString()));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Post), dto);
            }
        }

        [HttpPut]
        public IActionResult Put([FromBody] IdentityToken token)
        {
            try
            {
                return Ok(UserTokenService.IdentityTokenProvider.Update(Database, token, HttpContext.Connection.RemoteIpAddress.ToString()));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Put));
            }
        }

        [HttpDelete]
        public IActionResult Delete([FromBody] IdentityToken token)
        {
            try
            {
                return Ok(UserTokenService.IdentityTokenProvider.Delete(Database, token));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Delete));
            }
        }

        [HttpDelete("except")]
        [Authorize]
        public IActionResult DeleteAllExcept([FromBody] IdentityToken token)
        {
            try
            {
                return Ok(UserTokenService.IdentityTokenProvider.DeleteAllExcept(Database, User.GetId(), token));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(DeleteAllExcept), new { UserId = User.GetId()/*, Token = token*/ });
            }
        }
    }
}
