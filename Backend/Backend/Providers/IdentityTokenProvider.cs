﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Data.Repositories;
using Backend.Extensions.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Providers
{
    public class IdentityToken
    {
        //[NotLogged]
        [Required]
        public string AccessToken { get; set; }
        [Required]
        [StringLength(128, MinimumLength = 128)]
        public string RefreshToken { get; set; }
    }

    public class IdentityTokenProvider
    {
        private string Issuer { get; }
        private string Audience { get; }
        private TimeSpan Lifetime { get; }
        private byte[] Key { get; }
        private SymmetricSecurityKey SymmetricSecurityKey => new SymmetricSecurityKey(Key);
        private TimeSpan RefreshLifetime { get; }

        public UserTokenRepository UserTokenRepository { get; }

        public IdentityTokenProvider(IConfiguration configuration, UserTokenRepository userTokenRepository)
        {
            Issuer = configuration["Jwt:Issuer"];
            Audience = configuration["Jwt:Audience"];
            Lifetime = TimeSpan.Parse(configuration["Jwt:Lifetime"]);
            Key = Encoding.UTF8.GetBytes(configuration["Jwt:Key"]);
            RefreshLifetime = TimeSpan.Parse(configuration["Jwt:Refresh:Lifetime"]);

            UserTokenRepository = userTokenRepository;
        }

        private string GenerateToken(IEnumerable<Claim> claims)
        {
            var jwt = new JwtSecurityToken(
                issuer: Issuer,
                audience: Audience,
                notBefore: DateTime.UtcNow,
                claims: claims,
                expires: DateTime.UtcNow.Add(Lifetime),
                signingCredentials: new SigningCredentials(this.SymmetricSecurityKey, SecurityAlgorithms.HmacSha256)
            );

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        //public IdentityToken Create(DatabaseContext db, User user, string ipAddress) => Create(db, user.Id, user.Email, user.Roles, ipAddress);
        public IdentityToken Create(DatabaseContext db, int userId, string login, IEnumerable<string> roles, string ipAddress)
        {
            var identity = new ClaimsIdentity("Token");
            // JwtRegisteredClaimNames, ClaimsIdentity, ClaimTypes
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userId.ToString()));
            identity.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, login));
            identity.AddClaims(roles.Select(role => new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));

            return new IdentityToken() {
                AccessToken = GenerateToken(identity.Claims),
                RefreshToken = UserTokenRepository.Create(db, userId, UserTokenType.RefreshToken, ipAddress, expiresAt: DateTime.UtcNow.Add(RefreshLifetime)).Value
            };
        }

        private ClaimsPrincipal GetClaimsPrincipalFromExpiredToken(string accessToken)
        {
            var claimsPrincipal = new JwtSecurityTokenHandler().ValidateToken(accessToken, new TokenValidationParameters {
                ValidateIssuer = true,
                ValidIssuer = Issuer,
                ValidateAudience = true,
                ValidAudience = Audience,
                ValidateLifetime = false, // here we are saying that we don't care about the token's expiration date

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = this.SymmetricSecurityKey,
            }, out var securityToken);

            if (securityToken is JwtSecurityToken jwtSecurityToken && jwtSecurityToken.Header.Alg == SecurityAlgorithms.HmacSha256)
                return claimsPrincipal;

            throw new SecurityTokenException("Invalid token.");
        }

        public IdentityToken Update(DatabaseContext db, IdentityToken oldToken, string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(oldToken.RefreshToken))
                throw new SecurityTokenException("Invalid old refresh token.");

            var principal = GetClaimsPrincipalFromExpiredToken(oldToken.AccessToken);
            var userId = principal.GetId();

            return new IdentityToken() {
                AccessToken = GenerateToken(principal.Claims),
                RefreshToken = UserTokenRepository.Update(db, userId, UserTokenType.RefreshToken, oldToken.RefreshToken, ipAddress, expiresAt: DateTime.UtcNow.Add(RefreshLifetime)).Value
            };
        }

        public bool Delete(DatabaseContext db, IdentityToken token)
        {
            var principal = GetClaimsPrincipalFromExpiredToken(token.AccessToken);
            var userId = principal.GetId();

            return UserTokenRepository.Delete(db, userId, UserTokenType.RefreshToken, token.RefreshToken);
        }

        public int DeleteAllExcept(DatabaseContext db, int userId, IdentityToken token)
        {
            var principal = GetClaimsPrincipalFromExpiredToken(token.AccessToken);
            if (principal.GetId() != userId)
                throw new SecurityTokenException($"The token does not belong to user #{userId}.");

            return UserTokenRepository.DeleteAllExcept(db, userId, UserTokenType.RefreshToken, token.RefreshToken);
        }
    }
}
