﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Extensions.JwtBearer
{
    public static class ServiceCollectionExtension
    {
        public static AuthenticationBuilder AddJwtBearerAuthenticationExtension(this IServiceCollection services, IConfiguration configuration)
        {
            return services // https://github.com/aspnet/AspNetCore/issues/2007
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
#if (DEBUG)
                    options.RequireHttpsMetadata = false;
#endif
                    options.TokenValidationParameters = new TokenValidationParameters {
                        // укзывает, будет ли валидироваться издатель при валидации токена
                        ValidateIssuer = true,
                        // строка, представляющая издателя
                        ValidIssuer = configuration["Jwt:Issuer"],

                        // будет ли валидироваться потребитель токена
                        ValidateAudience = true,
                        // установка потребителя токена
                        ValidAudience = configuration["Jwt:Audience"],
                        // будет ли валидироваться время существования
                        ValidateLifetime = true,

                        // установка ключа безопасности
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"])),
                        // валидация ключа безопасности
                        ValidateIssuerSigningKey = true,

                        ClockSkew = TimeSpan.Zero // the default for this setting is 5 minutes
                    };
                });
        }
    }
}
