﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    interface IHttpException
    {
        int StatusCode { get; }
        string DefaultMessage { get; }
    }
}
