﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Backend.Data.Repositories
{
    public class EventRepository
    {
        public Event Create(DatabaseContext db, int userId, int membersLimit, string name, string description, string time, string location)
        {
            var entity = new Event() {
                UserId = userId,
                MembersLimit = membersLimit,
                Name = name,
                Description = description,
                Time = time,
                Location = location
            };
            db.Events.Add(entity);
            db.SaveChanges();
            return entity;
        }

        public Event Update(DatabaseContext db, int id, int userId, int membersLimit, string name, string description, string time, string location)
        {
            var @event = Read(db, id, userId);
            {
                @event.MembersLimit = membersLimit;
                @event.Name = name;
                @event.Description = description;
                @event.Time = time;
                @event.Location = location;
            }
            db.SaveChanges();
            return @event;
        }

        private Event Read(DatabaseContext db, int id, int userId)
        {
            return db.Events.Where(e => e.Id == id && e.UserId == userId).FirstOrDefault() ?? throw new HttpNotFoundException(nameof(Event), id);
        }

        public int ReadMembersLimit(DatabaseContext db, int id)
        {
            return db.Events.Where(e => e.Id == id).Select(e => new { e.MembersLimit }).FirstOrDefault()?.MembersLimit ?? throw new HttpNotFoundException(nameof(Event), id);
        }

        public Event Readonly(DatabaseContext db, int id)
        {
            return db.Events.AsNoTracking().Where(e => e.Id == id).FirstOrDefault() ?? throw new HttpNotFoundException(nameof(Event), id);
        }

        public List<Event> ReadonlyList(DatabaseContext db)
        {
            return db.Events.AsNoTracking().OrderByDescending(e => e.Id).ToList(); // TODO Skip & Take
        }

        public List<Event> ReadonlyList(DatabaseContext db, int userId)
        {
            return db.Events.AsNoTracking().Where(e => e.UserId == userId).OrderByDescending(e => e.Id).ToList(); // TODO Skip & Take
        }

        public bool ExistAny(DatabaseContext db, int id, int userId)
        {
            return db.Events.Any(e => e.Id == id && e.UserId == userId);
        }

        public bool Delete(DatabaseContext db, int id, int userId)
        {
            return db.EventMembers.Where(e => e.Id == id && e.UserId == userId).Delete() > 0;
        }
    }
}
