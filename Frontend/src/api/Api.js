import axios from "axios";
import {TokenStorage} from './Token';

export class ApiRequestArgs {
    /**
     * @type {?boolean}
     */
    anonymous = false;
    /**
     * @type {string}
     */
    method = 'GET';
    /**
     * @type {string}
     */
    url = '/';
    /**
     * @type {?Object}
     */
    headers = null;
    /**
     * @type {?Object}
     */
    params = null;
    /**
     * @type {?Object}
     */
    data = null;
    /**
     * @type {?string}
     */
    responseType = null;
}

export class ApiRequest {
    /**
     * @param {ApiRequestArgs} args
     * @param {?function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    constructor(args, thenFunction, catchFunction, finallyFunction) {
        this.args = args;
        this.thenFunction = thenFunction || (result => {
            console.info(`[${args.method.toUpperCase()}] ${args.url}`, result);
        });
        this.catchFunction = catchFunction || (error => {
            console.error(`[${args.method.toUpperCase()}] ${args.url}`, error);
            if (error.message) {
                if (typeof window.swal === 'function') {
                    window.swal(error.title || 'Error', error.message);
                } else {
                    alert(error.title || 'Error' + '\n' + error.message);
                }
            }
        });
        this.finallyFunction = finallyFunction || (() => {
            this.inProgress = false;
        });
    }

    /** @type {boolean} */
    inProgress = true;
    /** @type {ApiRequestArgs} */
    args;
    /** @type {function} */
    thenFunction;
    /** @type {function} */
    catchFunction;
    /** @type {function} */
    finallyFunction;
    /** @type {number} */
    attempt = 1;
}

export class Api {
    /**
     * @readonly
     * @type {string}
     */
    static Host = process.env.Api.Uri;

    /**
     * @private
     * @type {Array<ApiRequest>}
     */
    static _requests = [];

    /**
     * Send http request to api
     *
     * @param {ApiRequestArgs} args
     * @param {?function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     * @return {ApiRequest}
     */
    static Request(args, thenFunction = null, catchFunction = null, finallyFunction = null) {
        let request = new ApiRequest(args, thenFunction, catchFunction, finallyFunction);
        // add request to queue, if first - run next
        if (Api._requests.push(request) <= 1) {
            Api._nextRequest();
        }
        return request;
    }

    /** @private */
    static _nextRequest() {
        if (Api._requests.length <= 0) {
            return;
        }
        // get current request
        let request = Api._requests[0];
        if (process.env.Mode === 'development') {
            console.log('Request', request.args);
        }
        if (!request.args.anonymous && !TokenStorage.AccessToken) {
            return Api._toLoginPage(request, 'Token not found.');
        }
        Api._axiosRequest(request);
    }

    /** @private */
    static _nextRequestFinally() {
        // remove current request
        Api._requests.shift();
        /*if (process.env.Mode === 'development') {
            console.log('Request', request.args);
        }*/
        Api._nextRequest();
    }

    /** @private */
    static _axiosRequest(request) {
        if (request.attempt > 3) { // избегание бесконечного зацикливания
            return Api._toLoginPage(request, 'The number of attempts exceeded 3.');
        }
        axios({
            url: Api.Host + request.args.url,
            method: request.args.method,
            // 'Accept': 'application/json',
            // 'Content-Type': 'application/json',
            // 'Accept-Language': window.i18n?.locale || window.navigator?.language || 'en'
            headers: request.args.anonymous ? request.args.headers : Object.assign({'Authorization': 'Bearer ' + TokenStorage.AccessToken}, request.args.headers),
            params: request.args.params,
            data: request.args.data,
            responseType: request.args.responseType
        }).then(response => {
            request.thenFunction(response.data, response);
            request.finallyFunction(true);
            Api._nextRequestFinally();
        }).catch(error => {
            // Handle 401 error
            if (error.response && (error.response.status === 401 || error.response.statusText === 'Unauthorized')) {
                Api._handleUnauthorized(request);
            } else {
                Api._handleAxiosError(error, () => {
                    request.catchFunction(error);
                    request.finallyFunction(false);
                    Api._nextRequestFinally();
                });
            }
        });//.finally(finallyFunction);
    }

    /** @private */
    static _handleUnauthorized(request) {
        TokenStorage.RefreshToken(result => {
            if (result) {
                // send request again
                request.attempt++;
                Api._axiosRequest(request);
            } else {
                Api._toLoginPage(request, 'Token refresh fail.');
            }
        });
    }

    /** @private */
    static _handleAxiosError(error, finallyFunction) {
        if (error.response?.data instanceof Blob && error.response.data.type === 'application/json') {
            let reader = new FileReader();
            reader.onload = () => {
                let json = reader.result;
                error.response.data = JSON.parse(json);
                Api._printAxiosError(error);
                Api._modifyAxiosError(error);
                finallyFunction();
            };
            reader.readAsText(error.response.data, 'utf8');
        } else {
            Api._printAxiosError(error);
            Api._modifyAxiosError(error);
            finallyFunction();
        }
    }

    /** @private */
    static _printAxiosError(error) {
        if (error.response) {
            console.warn('The request was made and the server responded with a status code that falls out of the range of 2xx.');
            console.info(`Error ${error.response.status} (${error.response.statusText}):`);
            console.info('Config:', error.config);
            console.info('Headers:', error.response.headers);
            console.info('Data:', error.response.data);
        } else if (error.request) { // `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
            console.warn('The request was made but no response was received.');
            console.info('Error:', error.message);
            console.info('Error request:', error.request);
            console.info('Config:', error.config);
        } else {
            console.warn('Something happened in setting up the request that triggered an Error.');
            console.info('Error:', error.message);
            console.info('Config:', error.config);
        }
    }

    /** @private */
    static _modifyAxiosError(error) {
        if (!error.response) {
            error.title = 'Error';
            return;
        }
        if (error.response.data) {
            if (error.response.data.title === 'One or more validation errors occurred.') {
                error.title = 'Error ' + error.response.status + ': ' + error.response.data.title;
                error.message = '';
                for (let key in error.response.data.errors) {
                    if (key) {
                        error.message += `\r\n${key}: ${error.response.data.errors[key]}`;
                    } else {
                        error.message += '\r\n' + error.response.data.errors[key];
                    }
                }
                return;
            }
            if (error.response.data.message) {
                error.title = 'Error ' + error.response.status + ': ' + error.message;
                error.message = error.response.data.message;
                return;
            }
        }
        if (error.response.statusText) {
            error.title = 'Error ' + error.response.status + ': ' + error.message;
            error.message = error.response.statusText;
        } else {
            error.title = 'Error ' + error.response.status;
        }
    }

    /** @private */
    static _toLoginPage(request, error) {
        request.finallyFunction(false);
        // clear request queue
        Api.Requests = [];
        // clear token
        TokenStorage.TokenObject = null;
        // error
        if (error) {
            console.error(`[${request.args.method.toUpperCase()}] ${request.args.url}`, error);
            if (process.env.Mode === 'development') {
                alert(`[${request.args.method.toUpperCase()}] ${request.args.url}: ${error}`); // page pause
            }
        }
        // to login page
        window.location = '/login?redirect=' + encodeURIComponent(window.location.pathname + window.location.search);
    }

    /**
     * @param {object} object
     * @return {FormData}
     */
    static ObjectToFormData(object) {
        let formData = new FormData();
        Object.keys(object).forEach(key => {
            let value = object[key];
            formData.append(key, (value === null || value === undefined || Number.isNaN(value)) ? '' : value);
        });
        return formData;
    }

    /**
     * @param {object} object
     * @param {function} entity
     * @return {object}
     */
    static ObjectToEntity(object, entity) {
        // https://jeena.net/constructor-object-create
        let obj = object ? new entity(object) : null;//Object.create(entity.prototype);
        //entity.apply(obj, object);
        return obj;
    }

    /**
     * @param {array} objects
     * @param {function} entity
     */
    static ListToEntities(objects, entity) {
        // modify simple array
        objects.forEach((object, index) => {
            objects[index] = Api.ObjectToEntity(object, entity);
        });
        return objects;
    }

    /**
     * https://gist.github.com/javilobo8/097c30a233786be52070986d8cdb1743
     * https://github.com/eligrey/FileSaver.js
     * @param {Blob} blob
     */
    static BlobToFile(blob) {
        let url = window.URL.createObjectURL(blob);
        let link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', '');
        document.body.appendChild(link);
        link.click();
        link.remove();
        window.URL.revokeObjectURL(url);
    }
}