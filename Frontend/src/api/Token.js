import Vue from 'vue';
import UserController from './controllers/UserController';
import UserTokenController from "$api/controllers/UserTokenController";

export class IdentityToken {
    constructor(entity = null) {
        if (entity) {
            return Object.assign(new IdentityToken(), entity);
        }
    }
    /**
     * @type string
     */
    accessToken;
    /**
     * @type string
     */
    refreshToken;
}

export class TokenStorage {
    /**
     * @returns {?IdentityToken}
     */
    static get TokenObject() {
        return Vue.prototype.$storage.get('token', null);
    }

    /**
     * @param {IdentityToken} value
     */
    static set TokenObject(value) {
        if (value) {
            Vue.prototype.$storage.set('token', value, {ttl: parseInt(process.env.Api.Token.Lifetime)});
        } else {
            Vue.prototype.$storage.remove('token');
        }
    }

    /**
     * @returns {?string}
     */
    static get AccessToken() {
        return Vue.prototype.$storage.get('token', null)?.accessToken;
    }

    /**
     * @param {function} resultFunction
     */
    static RefreshToken(resultFunction) {
        let oldToken = TokenStorage.TokenObject;

        if (!oldToken) {
            return resultFunction(null);
        }

        if (process.env.Mode !== 'production') {
            console.log('token be old', oldToken);
        }

        UserTokenController.Put(oldToken, (newToken) => {
            if (newToken.accessToken && newToken.refreshToken) {
                // update token
                TokenStorage.TokenObject = newToken;
                if (process.env.Mode !== 'production') {
                    console.log('token updated', newToken);
                }
                resultFunction(true);
            } else {
                resultFunction(false);
            }
        }, error => {
            console.error(error.message);
            resultFunction(false);
        });
    }
}
