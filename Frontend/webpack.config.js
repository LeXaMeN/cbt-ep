// const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const {SourceMapDevToolPlugin} = require('webpack');
const {VueLoaderPlugin} = require('vue-loader');
const Dotenv = require('dotenv-webpack');
const path = require('path');

module.exports = function (env, argv) {
    return {
        entry: {
            index: path.resolve('src', 'app.js'),
            // admin: path.resolve('public', 'admin', 'app.js')
        },
        output: {
            path: path.resolve('dist', 'build'),
            filename: '[name].js'
        },
        // https://webpack.js.org/configuration/devtool
        devtool: env.Mode === 'production' ? false : 'inline-source-map',
        resolve: {
            extensions: ['.js', '.jsx', '.vue'],
            alias: {
                $api: path.resolve('src', 'api'),
                $src: path.resolve('src'),
                $lib: path.resolve('lib')
            }
        },
        module: {
            rules: [
                // Vue Files
                {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                },
                // JS Files
                {
                    test: /\.jsx?$/,
                    loader: 'babel-loader',
                    include: [
                        path.resolve('lib'),
                        path.resolve('src'),
                        path.resolve('public')
                    ],
                    // query: {presets: ['es2015']}
                },
                // CSS Files
                {
                    test: /\.s?css$/,
                    use: [
                        'vue-style-loader',
                        'css-loader',
                        // {loader: 'sass-loader'},
                    ],
                },
                // Images
                {
                    test: /\.(gif|png|jpe?g|svg|woff2?|eot|ttf)$/i,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {name: '/build/img/[hash].[ext]'}
                        },
                        {
                            loader: 'image-webpack-loader',
                            // options: {disable: true}
                        }
                    ]
                },
                // Exposing
                {
                    test: require.resolve('jquery'),
                    use: [
                        {loader: "expose-loader", options: {exposes: ["$", "jQuery"]}}
                    ]
                }
            ]
        },
        plugins: [
            new VueLoaderPlugin(),
            // https://momentjs.com/docs/#/use-it/webpack/
            // new MomentLocalesPlugin({localesToKeep: ['ru']}),
            // https://github.com/mrsteele/dotenv-webpack
            new Dotenv({
                path: env.Mode === 'production' ? './.env.production' : './.env',
                safe: true // .env.example
            }),
            // https://webpack.js.org/plugins/source-map-dev-tool-plugin
            env.Mode === 'production' ? new SourceMapDevToolPlugin({
                filename: '[file].map',
                publicPath: 'http://ep.loc/build/'
            }) : null
        ].filter(Boolean)
    }
};
