﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    public class HttpBadRequestException : Exception, IHttpException
    {
        public int StatusCode => (int)HttpStatusCode.BadRequest;
        public string DefaultMessage => "Invalid request data.";

        public HttpBadRequestException(string message) : base(message) { }
        public HttpBadRequestException(string message, Exception innerException) : base(message, innerException) { }
    }
}
