import VeeValidate from 'vee-validate';

// https://vee-validate.logaretm.com/v2/guide/rules.html
export default class VueValidate {
    static install = function (Vue, options) {
        window.addEventListener('load', () => {
            window.App.$validator.__proto__.validateTask = function (thenFunction, catchFunction = null, finallyFunction = null) {
                this.validate().then(valid => {
                    if (valid) {
                        thenFunction();
                    } else {
                        if (typeof catchFunction === 'function') {
                            catchFunction(this.errors.all());
                        } else {
                            let error = this.errors.all().join('.\n') + '.';
                            if (typeof window.swal === 'function') {
                                window.swal(error);
                            } else {
                                alert(error);
                            }
                        }
                    }
                    if (typeof finallyFunction === 'function') {
                        finallyFunction(valid);
                    }
                });
            };
            window.App.$validator.__proto__.validateAllTask = function (scope, thenFunction, catchFunction = null, finallyFunction = null) {
                this.validateAll(scope).then(valid => {
                    if (valid) {
                        thenFunction();
                    } else {
                        if (typeof catchFunction === 'function') {
                            catchFunction(this.errors.all(scope));
                        } else {
                            let error = this.errors.all(scope).join('.\n') + '.';
                            if (typeof window.swal === 'function') {
                                window.swal(error);
                            } else {
                                alert(error);
                            }
                        }
                    }
                    if (typeof finallyFunction === 'function') {
                        finallyFunction(valid);
                    }
                });
            };
        });
        // http://vee-validate.logaretm.com/v2/api/
        Vue.use(VeeValidate, options);
    }
}
