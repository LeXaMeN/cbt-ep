export default class VueRedirect {
    static install = function (Vue, options) {
        Vue.prototype.$redirect = {
            // uri: function (uri) {
            //     return (options.url || window.location.origin) + '?' + (options.param || 'ref') + '=' + encodeURIComponent(uri);
            // },
            to(uri) {
                window.open(uri);
                // window.location.href = this.uri(uri);
            },
            toLoginPage() {
                window.location.href = this.uriLogin;
            },
            get uriLogin() {
                return '/login?redirect=' + encodeURIComponent(window.location.pathname + window.location.search);
            },
            // get uriRegister() {
            //     return '/register?redirect=' + encodeURIComponent(window.location.pathname + window.location.search);
            // }
        }
    }
}
