import {Api} from "$api/Api";
import {IdentityToken} from "$api/Token";
import axios from "axios";

export default class UserTokenController {
    /**
     * @param {string} login
     * @param {string} password
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Post(login, password, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            anonymous: true,
            method: 'post',
            url: '/user-tokens',
            data: {
                'login': login,
                'password': password
            },
        }, (object) => thenFunction(Api.ObjectToEntity(object, IdentityToken)), catchFunction, finallyFunction);
    }

    /**
     * @param {IdentityToken} oldToken
     * @param {function} thenFunction
     * @param {function} catchFunction
     */
    static Put(oldToken, thenFunction, catchFunction) {
        // IMPORTANT: don't move to Api class
        axios({
            url: Api.Host + '/user-tokens',
            method: 'put',
            data: oldToken
        }).then(response => {
            let newToken = Api.ObjectToEntity(response.data, IdentityToken);
            if (newToken) {
                thenFunction(newToken);
            } else {
                // console.warn('IdentityToken not found in body.');
                catchFunction({message: 'IdentityToken not found in body.'});
            }
        }).catch(catchFunction);
    }

    /**
     * @param {IdentityToken} token
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Delete(token, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            anonymous: true,
            method: 'delete',
            url: '/user-tokens',
            data: token
        }, thenFunction, catchFunction, finallyFunction);
    }
}