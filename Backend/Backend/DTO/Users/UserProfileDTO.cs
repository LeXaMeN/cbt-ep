﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Users
{
    public class UserProfileDTO
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [StringLength(128)]
        public string Surname { get; set; }
        [Required]
        [Range(1, 255)]
        public int Age { get; set; }
    }
}
