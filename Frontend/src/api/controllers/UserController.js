import {Api} from '$api/Api';
import {IdentityToken} from '$api/Token';
import User from '$api/entity/User';

export default class UserController {
    /**
     * @param {string} login
     * @param {string} password
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Post(login, password, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            anonymous: true,
            method: 'post',
            url: '/users',
            data: {
                'login': login,
                'password': password
            },
        }, thenFunction, catchFunction, finallyFunction);
    }

    /**
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Get(thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'get',
            url: '/users',
        }, (object) => thenFunction(Api.ObjectToEntity(object, User)), catchFunction, finallyFunction);
    }
}
