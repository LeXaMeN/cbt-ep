﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Extensions.StartupTask
{
    public static class HostExtension
    {
        /*public static Task RunAfterStartupTasksAsync(this IHost host, CancellationToken cancellationToken = default)
        {
            var startupTasks = host.Services.GetServices<IStartupTask>();

            Task.WaitAll(startupTasks.Select(st => st.ExecuteAsync(cancellationToken)).ToArray(), cancellationToken);

            return host.RunAsync(cancellationToken);
        }*/

        public static void RunAfterStartupTasks(this IHost host)
        {
            var startupTasks = host.Services.GetServices<IStartupTask>();

            foreach (var startupTask in startupTasks)
            {
                startupTask.Execute();
            }

            host.Run();
        }
    }
}
