﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    /// <summary>
    /// Оболочка для добавления дополнительных данных для логгирования.
    /// Нужно вызывать throw с этой ошибкой только в контроллерах!
    /// </summary>
    public class HttpResponseException : Exception, IHttpException
    {
        public int StatusCode => (int)HttpStatusCode.InternalServerError;
        public string DefaultMessage => "An error occurred on the server.";

        public LogLevel Level { get; set; } = LogLevel.Error;
        public string Method { get; }
        public object[] Arguments { get; }

        public HttpResponseException(Exception innerException, string method, params object[] args) : base(innerException.Message, innerException)
        {
            Method = method;
            Arguments = args;
        }
    }
}
