/**
 * Convert to money style
 *
 * @param number
 * @param min
 * @return {number}
 */
Math.toMoney = function (number, min = 0) {
    return Math.max(Math.round(number * 100) / 100, min);
};

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
Math.randomInt = function (min, max) {
    min = Math.ceil(min);
    return Math.floor(Math.random() * (Math.floor(max) - min) + min); //The maximum is exclusive and the minimum is inclusive
};

window.MathD = {
    /**
     * @param {number} x
     * @param {number} digits
     * @return {number}
     */
    ceil: function (x, digits) {
        let adjustment = Math.pow(10, digits);
        return Math.ceil(x * adjustment) / adjustment;
    },
    /**
     * @param {number} x
     * @param {number} digits
     * @return {number}
     */
    floor: function (x, digits) {
        let adjustment = Math.pow(10, digits);
        return Math.floor(x * adjustment) / adjustment;
    },
    /**
     * @param {number} x
     * @param {number} digits
     * @return {number}
     */
    round: function (x, digits) {
        let adjustment = Math.pow(10, digits);
        return Math.round(x * adjustment) / adjustment;
    },

    /**
     * @param {number} min
     * @param {number} max
     * @return {number}
     */
    random: function (min, max) {
        return Math.floor(Math.random() * max) + min;
    }
};
