﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(Event))]
    public class Event
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int MembersLimit { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }
        [MaxLength(4096)]
        public string Description { get; set; }

        [MaxLength(128)]
        public string Time { get; set; }
        [MaxLength(256)]
        public string Location { get; set; }
    }
}
