﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Backend.Extensions.Database
{
    public static class ModelBuilderExtension
    {
        public static ModelBuilder UseCoordinatedUniversalTimeConversion(this ModelBuilder builder)
        {
            // https://github.com/aspnet/EntityFrameworkCore/issues/4711#issuecomment-481215673
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(DateTime))
                    {
                        builder.Entity(entityType.ClrType).Property<DateTime>(property.Name).HasConversion(
                            v => (v.Kind == DateTimeKind.Utc) ? v : (v.Kind == DateTimeKind.Local) ? v.ToUniversalTime() : DateTime.SpecifyKind(v, DateTimeKind.Utc),
                            v => DateTime.SpecifyKind(v, DateTimeKind.Utc)
                        );
                    }
                    else if (property.ClrType == typeof(DateTime?))
                    {
                        builder.Entity(entityType.ClrType).Property<DateTime?>(property.Name).HasConversion(
                            v => v.HasValue ? ((v.Value.Kind == DateTimeKind.Utc) ? v.Value : (v.Value.Kind == DateTimeKind.Local) ? v.Value.ToUniversalTime() : DateTime.SpecifyKind(v.Value, DateTimeKind.Utc)) : v,
                            v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v
                        );
                    }
                }
            }

            return builder;
        }
    }
}
