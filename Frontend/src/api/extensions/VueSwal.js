import swal from 'sweetalert';

export default class VueSwal {
    static install = function (Vue, options) {
        // https://www.npmjs.com/package/sweetalert
        // https://sweetalert.js.org/guides/#advanced-examples
        // https://github.com/t4t5/sweetalert/blob/18a05a6c07a14cb09c3c0ec364ab6de3c7ffeae6/index.html
        Vue.prototype.$swal = window.swal = swal;
    }
}
