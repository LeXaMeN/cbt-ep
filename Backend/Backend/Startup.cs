using Backend.Data.Context;
using Backend.Data.Repositories;
using Backend.Enums;
using Backend.Extensions.Cors;
using Backend.Extensions.JwtBearer;
using Backend.Extensions.Serializer;
using Backend.Extensions.StartupTask;
using Backend.Extensions.StartupTask.Tasks;
using Backend.Middleware;
using Backend.Providers;
using Backend.Services;
using Backend.Services.Background;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var defaultConnection = Configuration.GetConnectionString("DefaultConnection");
            // https://stackoverflow.com/a/48444206
            services.AddDbContextPool<DatabaseContext>(options => options
                //.UseLoggerFactory(LoggerFactory)
#if (DEBUG)
                .EnableSensitiveDataLogging() // https://stackoverflow.com/a/44207235
                //.ConfigureWarnings(bulder => bulder.Throw(RelationalEventId.QueryClientEvaluationWarning))
#endif
                .UseMySql(defaultConnection, ServerVersion.AutoDetect(defaultConnection))
            );

            // Middleware
            services.AddTransient<HttpExceptionHandlerMiddleware>();

            // Repositories
            services.AddSingleton<EventRepository>();
            services.AddSingleton<EventMemberRepository>();
            services.AddSingleton<UserRepository>();
            services.AddSingleton<UserProfileRepository>();
            services.AddSingleton<UserTokenRepository>();

            // Providers
            services.AddSingleton<EmailProvider>();
            services.AddSingleton<IdentityTokenProvider>();

            // Services
            services.AddSingleton<EventService>();
            services.AddSingleton<EventMemberService>();
            services.AddSingleton<UserService>();
            services.AddSingleton<UserTokenService>();

            // Background services
            services.AddHostedService<UserTokenBackgroundService>();

            // Token autorization
            services.AddJwtBearerAuthenticationExtension(Configuration);

            // https://stackoverflow.com/a/39113342
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddCorsExtension(Configuration);

            services.AddControllers()
                .AddJsonCamelCaseOptionsExtension();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Backend", Version = "v1" });
            });

            // Startup tasks
            services.AddStartupTask<MigrationStartupTask>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Backend v1"));
            }
#if DEBUG
            app.UseHttpsRedirection();
            app.UseHsts();
#endif
            app.UseRouting();
            app.UseCors(PolicyName.Default);
            app.UseAuthentication();
            app.UseAuthorization();
#if !DEBUG
            // https://www.jerriepelser.com/blog/aspnetcore-geo-location-from-ip-address/
            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                ForwardLimit = 2
            });
#endif
            //app.UseRequestLocalizationExtension();
            app.UseMiddleware<HttpExceptionHandlerMiddleware>();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
