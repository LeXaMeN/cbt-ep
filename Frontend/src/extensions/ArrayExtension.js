/**
 * Remove and returns the first element in the array where predicate is true, and undefined otherwise.
 *
 * @param {function} predicate
 * @param {boolean} warn
 * @return {any|undefined}
 */
Array.prototype.remove = function (predicate, warn = true) {
    let index = this.findIndex(predicate);
    if (index >= 0) {
        return this.splice(index, 1)[0];
    } else {
        if (warn) {
            console.warn('Failed to remove item by predicate.', predicate);
        }
        return undefined;
    }
};
/**
 * @param {function} predicate
 * @param {any} item
 * @param {boolean} warn
 * @return {boolean}
 */
Array.prototype.replace = function (predicate, item, warn = true) {
    let index = this.findIndex(predicate);
    if (index >= 0) {
        // this[index] = item;
        this.splice(index, 1, item);
        return true;
    } else {
        if (warn) {
            console.warn('Failed to replace item by predicate.', predicate);
        }
        return false;
    }
};
/**
 * Only implement if no native implementation is available
 */
if (typeof Array.isArray === 'undefined') {
    /**
     * @param {Array<any>} arg
     * @return {boolean}
     */
    Array.isArray = function (arg) {
        return Object.prototype.toString.call(arg) === '[object Array]';
    };
}
/**
 * Sort object array by param names.
 *
 * @param {string} paramNames
 */
Array.prototype.sortBy = function (...paramNames) {
    return this.sort(function (a, b) {
        for (let i = 0; i < paramNames.length; i++) {
            if (typeof a[paramNames[i]] === 'string' && typeof b[paramNames[i]] === 'string') {
                return a[paramNames[i]].localeCompare(b[paramNames[i]]);
            } else if (a[paramNames[i]] - b[paramNames[i]]) {
                return (a[paramNames[i]] - b[paramNames[i]]);
            }
        }
        return 0;
    });
};
/**
 * Sort object array by param names.
 *
 * @param {string} paramNames
 */
Array.prototype.sortByDesc = function (...paramNames) {
    return this.sort(function (a, b) {
        for (let i = 0; i < paramNames.length; i++) {
            if (typeof b[paramNames[i]] === 'string' && typeof a[paramNames[i]] === 'string') {
                return b[paramNames[i]].localeCompare(a[paramNames[i]]);
            } else if (b[paramNames[i]] - a[paramNames[i]]) {
                return (b[paramNames[i]] - a[paramNames[i]]);
            }
        }
        return 0;
    });
};
/**
 * @param {Function} selector
 * @param {?number} emptyValue
 * @return {?number}
 */
Array.prototype.min = function (selector, emptyValue = null) {
    if (this.length === 0) {
        return emptyValue;
    }
    return this.reduce(function (accumulator, currentValue/*, index, array*/) {
        return selector(currentValue) < accumulator ? selector(currentValue) : accumulator;
    }, selector(this[0]));
};
/**
 * @param {Function} selector
 * @param {?number} emptyValue
 * @return {?number}
 */
Array.prototype.max = function (selector, emptyValue = null) {
    if (this.length === 0) {
        return emptyValue;
    }
    return this.reduce(function (accumulator, currentValue/*, index, array*/) {
        return selector(currentValue) > accumulator ? selector(currentValue) : accumulator;
    }, selector(this[0]));
};
/**
 * @param {Function} selector
 * @param {number} initialValue
 * @return {number}
 */
Array.prototype.sum = function (selector, initialValue = 0) {
    return this.reduce(function (accumulator, currentValue/*, index, array*/) {
        return accumulator + selector(currentValue);
    }, initialValue);
};

/**
 * @return {?object}
 */
Array.prototype.first = function () {
    return this.length > 0 ? this[0] : null;
};
/**
 * @return {?object}
 */
Array.prototype.last = function () {
    return this.length > 0 ? this[this.length - 1] : null;
};
/**
 * @return {*[]}
 */
Array.prototype.unique = function () {
    return this.filter(function (value, index, _this) {
        return _this.indexOf(value) === index;
    });
};
