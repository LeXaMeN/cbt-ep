﻿using Backend.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Utils.Security.Cryptography
{
    public class Hash
    {
        // https://www.kunal-chowdhury.com/2016/01/compute-hash-code.html
        public enum Algorithm
        {
            MD5,
            SHA1,
            SHA256,
            SHA384,
            SHA512,
            //Keccak_224,
            //Keccak_256,
            //Keccak_384,
            //Keccak_512
        }

        private static HashAlgorithm CryptoServiceProvider(Algorithm algorithm)
        {
            switch (algorithm)
            {
                case Algorithm.MD5:
                    return new MD5CryptoServiceProvider();
                case Algorithm.SHA1:
                    return new SHA1CryptoServiceProvider();
                case Algorithm.SHA256:
                    return new SHA256CryptoServiceProvider();
                case Algorithm.SHA384:
                    return new SHA384CryptoServiceProvider();
                case Algorithm.SHA512:
                    return new SHA512CryptoServiceProvider();
                //case Algorithm.Keccak_224:
                //    return new KeccakManaged(224);
                //case Algorithm.Keccak_256:
                //    return new KeccakManaged(256);
                //case Algorithm.Keccak_384:
                //    return new KeccakManaged(384);
                //case Algorithm.Keccak_512:
                //    return new KeccakManaged(512);
                default:
                    throw new ArgumentOutOfRangeException(nameof(algorithm), algorithm, null);
            }
        }

        public static string File(string path, Algorithm algorithm)
        {
            using (var cryptoService = CryptoServiceProvider(algorithm))
            using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return cryptoService.ComputeHash(fileStream).ToHex();
            }
        }

        public static string Stream(Stream stream, Algorithm algorithm)
        {
            using (var cryptoService = CryptoServiceProvider(algorithm))
            {
                return cryptoService.ComputeHash(stream).ToHex();
            }
        }

        public static string Bytes(byte[] bytes, Algorithm algorithm)
        {
            using (var cryptoService = CryptoServiceProvider(algorithm))
            {
                return cryptoService.ComputeHash(bytes).ToHex();
            }
        }

        public static string String(string @string, Algorithm algorithm)
        {
            return Bytes(Encoding.UTF8.GetBytes(@string), algorithm);
        }

        public static string Random(Algorithm algorithm)
        {
            byte[] bytes = null;

            switch (algorithm)
            {
                case Algorithm.MD5:
                    bytes = new byte[32];
                    break;
                case Algorithm.SHA1:
                    bytes = new byte[40];
                    break;
                case Algorithm.SHA256:
                    //case Algorithm.Keccak_256:
                    bytes = new byte[64];
                    break;
                case Algorithm.SHA384:
                    //case Algorithm.Keccak_384:
                    bytes = new byte[96];
                    break;
                case Algorithm.SHA512:
                    //case Algorithm.Keccak_512:
                    bytes = new byte[128];
                    break;
                //case Algorithm.Keccak_224:
                //    bytes = new byte[28];
                //    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(Algorithm), algorithm, null);
            }

            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(bytes);
            }

            return Bytes(bytes, algorithm);
        }
    }
}
