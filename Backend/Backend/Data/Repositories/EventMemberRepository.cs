﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Data.Repositories
{
    public class EventMemberRepository
    {
        public EventMember Create(DatabaseContext db, int userId, int eventId)
        {
            var entity = ReadonlyOrDefault(db, userId, eventId);
            if (entity == null)
            {
                entity = new EventMember() {
                    EventId = eventId,
                    UserId = userId,
                    CreatedAt = DateTime.UtcNow
                };
                db.EventMembers.Add(entity);
                db.SaveChanges();
            }
            return entity;
        }

        public bool ExistAny(DatabaseContext db, int userId, int eventId)
        {
            return db.EventMembers.Any(m => m.EventId == eventId && m.UserId == userId);
        }

        public EventMember ReadonlyOrDefault(DatabaseContext db, int userId, int eventId)
        {
            return db.EventMembers.Where(m => m.EventId == eventId && m.UserId == userId).FirstOrDefault();
        }

        public List<EventMember> ReadonlyList(DatabaseContext db, /*int userId,*/ int eventId)
        {
            return db.EventMembers.Where(m => m.EventId == eventId).ToList();
        }

        public int ReadCount(DatabaseContext db, int eventId)
        {
            return db.EventMembers.Where(m => m.EventId == eventId).Count();
        }

        public bool Delete(DatabaseContext db, int userId, int eventId)
        {
            return db.EventMembers.Where(m => m.EventId == eventId && m.UserId == userId).Delete() > 0;
        }

        public bool DeleteAll(DatabaseContext db, int eventId)
        {
            return db.EventMembers.Where(m => m.EventId == eventId).Delete() > 0;
        }
    }
}
