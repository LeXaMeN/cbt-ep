# Event planning

### Backend
- Установите MySQL и создайте бд командой ```CREATE SCHEMA `ep` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci```
- Настройте файл ```appsettings.Development```, в частности поля ```ConnectionStrings.DefaultConnection``` и ```Api:Url```
- Настройте "перехватчик email", к примеру используя ```mailtrap.io```

### Frontend
- Установите nginx и добавьте следующий конфигурационный файл со своими данными:
```
server {
  listen 80;
  listen [::]:80;

  root F:/Projects/Cbt/Frontend/dist;

  index index.html;

  server_name ep.loc;

  location / {
    try_files $uri $uri/ @rewrites;
  }

  location @rewrites {
    rewrite ^(.+)$ /index.html last;
  }

  location ~* \.(?:ico|css|js|gif|jpe?g|png|woff?2|ttf)$ {
    # Some basic cache-control for static files to be sent to the browser
    expires max;
    add_header Pragma public;
    add_header Cache-Control "public, must-revalidate, proxy-revalidate";
  }

  error_log D:/alex/Logs/ep.error.log;
  access_log D:/alex/Logs/ep.access.log;
}
```
- Добавьте запись в ```hosts``` файл: ```127.0.0.1 ep.loc```
- Измените параметр ```Api.Uri``` в файле ```.env``` фронтенда
- Запустите ```npm run dev``` в папке с фронтендом

1. Запустите C# проект и Nginx.
2. Откройте в браузере сайт http://ep.loc
3. Проект должен работать.
