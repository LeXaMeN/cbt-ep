/**
 * @param {number?} fractionDigits
 * @return {number}
 */
Number.prototype.toUnfixed = function (fractionDigits) {
    if (typeof fractionDigits === 'number') {
        let adjustment = Math.pow(10, fractionDigits);
        return (Math.round(this * adjustment) / adjustment);//.toString();
    }
    return Math.round(this);//.toString();
};
