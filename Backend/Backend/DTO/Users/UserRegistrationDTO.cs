﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Users
{
    public class UserRegistrationDTO
    {
        [Required]
        [StringLength(255, MinimumLength = 6)]
        [EmailAddress]
        public string Login { get; set; }

        //[NotLogged]
        [Required]
        [StringLength(64, MinimumLength = 8)]
        public string Password { get; set; }
    }
}
