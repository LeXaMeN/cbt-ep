﻿using Backend.Data.Context;
using Backend.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class EventService
    {
        public EventRepository EventRepository { get; }
        public EventMemberRepository EventMemberRepository { get; }

        public EventService(EventRepository eventRepository, EventMemberRepository eventMemberRepository)
        {
            EventRepository = eventRepository;
            EventMemberRepository = eventMemberRepository;
        }

        public bool Delete(DatabaseContext db, int userId, int eventId)
        {
            if (!EventRepository.ExistAny(db, eventId, userId))
                return false;

            EventMemberRepository.DeleteAll(db, eventId);
            return EventRepository.Delete(db, eventId, userId);
        }
    }
}
