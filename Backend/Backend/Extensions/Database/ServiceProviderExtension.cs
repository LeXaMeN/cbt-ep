﻿using Backend.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Extensions.Database
{
    public static class ServiceProviderExtension
    {
        public static IServiceScope CreateScope(this IServiceProvider provider, out DatabaseContext db)
        {
            var scope = provider.CreateScope();
            db = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            return scope;
        }
    }
}
