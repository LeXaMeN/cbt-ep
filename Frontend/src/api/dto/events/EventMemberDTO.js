export default class EventMemberDTO {
    constructor(entity = null) {
        if (entity) {
            return Object.assign(new EventMemberDTO(), entity);
        }
    }

    /**
     * @type string
     */
    name;
    /**
     * @type string
     */
    surname;
    /**
     * @type number
     */
    age;
}