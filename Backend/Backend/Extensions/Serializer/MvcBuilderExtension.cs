﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Extensions.Serializer
{
    public static class MvcBuilderExtension
    {
        public static IMvcBuilder AddJsonCamelCaseOptionsExtension(this IMvcBuilder builder)
        {
            return builder.AddNewtonsoftJson(options =>
            {
                // exception serialize bug fix
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver() { IgnoreSerializableInterface = true };
                // https://stackoverflow.com/a/18223985
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
        }
    }
}
