export default class UserProfileDTO {

    /**
     * @param {string} name
     * @param {string} surname
     * @param {number} age
     */
    constructor(name, surname, age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    /**
     * @type string
     */
    name;
    /**
     * @type string
     */
    surname;
    /**
     * @type number
     */
    age;
}