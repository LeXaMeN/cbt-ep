﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Data.Repositories;
using Backend.DTO.Events;
using Backend.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class EventMemberService
    {
        public EventRepository EventRepository { get; }
        public EventMemberRepository EventMemberRepository { get; }
        public UserProfileRepository UserProfileRepository { get; }

        public EventMemberService(EventRepository eventRepository, EventMemberRepository eventMemberRepository, UserProfileRepository userProfileRepository)
        {
            EventRepository = eventRepository;
            EventMemberRepository = eventMemberRepository;
            UserProfileRepository = userProfileRepository;
        }

        public bool Create(DatabaseContext db, int userId, int eventId)
        {
            if (EventMemberRepository.ExistAny(db, userId, eventId))
                return false;

            var membersLimit = EventRepository.ReadMembersLimit(db, eventId);
            var membersCount = EventMemberRepository.ReadCount(db, eventId);
            if (membersCount >= membersLimit)
                throw new HttpBadRequestException("Members limit reached.");

            EventMemberRepository.Create(db, userId, eventId);
            return true;
        }

        public List<EventMemberDTO> ReadonlyList(DatabaseContext db, int userId, int eventId)
        {
            if (!EventRepository.ExistAny(db, eventId, userId))
                throw new HttpNotFoundException(nameof(Event), eventId);

            var members = EventMemberRepository.ReadonlyList(db, eventId);
            return members.Select(m => UserProfileRepository.ReadonlyOrDefault(db, m.UserId)).Select(p => new EventMemberDTO() {
                Name = p.Name,
                Surname = p.Surname,
                Age = p.Age
            }).ToList();
        }
    }
}
