// import VueMoment from 'vue-moment';
import moment from 'moment';

// https://www.npmjs.com/package/vue-moment#configuration
// require('moment/locale/en'); used by default
//require('moment/locale/ru');

export default class {
    static install = function (Vue, options) {
        // https://www.npmjs.com/package/vue-moment
        // Vue.use(VueMoment, Object.assign({moment}, options));
        window.moment = Vue.prototype.$moment = moment;
        window.moment.prototype.defaultDatetimeFormat = 'YYYY-DD-MM HH:mm:ss';
        window.moment.prototype.toDefaultString = function () {
            return this.format('YYYY-DD-MM HH:mm:ss');
        };
        window.moment.prototype.defaultDateFormat = 'YYYY-DD-MM';
        window.moment.prototype.toDefaultDateString = function () {
            return this.format('YYYY-DD-MM');
        };
    }
}
