import {Api} from '$api/Api';
import Event from '$api/entity/Event';
import EventDTO from '$api/dto/events/EventDTO';

export default class EventController {
    /**
     * @param {EventDTO} eventDTO
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Post(eventDTO, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'post',
            url: '/events',
            data: eventDTO
        }, (object) => thenFunction(Api.ObjectToEntity(object, Event)), catchFunction, finallyFunction);
    }

    /**
     * @param {number} eventId
     * @param {EventDTO} eventDTO
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Put(eventId, eventDTO, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'put',
            url: '/events/' + eventId,
            data: eventDTO
        }, (object) => thenFunction(Api.ObjectToEntity(object, Event)), catchFunction, finallyFunction);
    }

    /**
     * @param {number} eventId
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Get(eventId, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'get',
            url: '/events/' + eventId
        }, (object) => thenFunction(Api.ObjectToEntity(object, Event)), catchFunction, finallyFunction);
    }

    /**
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static GetMyList(thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'get',
            url: '/events/my'
        }, (list) => thenFunction(Api.ListToEntities(list, Event)), catchFunction, finallyFunction);
    }

    /**
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static GetList(thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'get',
            url: '/events'
        }, (list) => thenFunction(Api.ListToEntities(list, Event)), catchFunction, finallyFunction);
    }

    /**
     * @param {number} eventId
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Delete(eventId, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'delete',
            url: '/events/' + eventId
        }, thenFunction, catchFunction, finallyFunction);
    }
}
