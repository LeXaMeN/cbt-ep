﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Data.Repositories
{
    public class UserRepository
    {
        public User Create(DatabaseContext db, string login, string password)
        {
            if (db.Users.Any(u => u.Login == login))
                throw new ArgumentException($"Login {login} already exists.");

            var user = new User() {
                Login = login,
                PasswordHash = new PasswordHasher<User>().HashPassword(null, password),
                CreatedAt = DateTime.UtcNow
            };

            db.Users.Add(user);
            db.SaveChanges();

            return user;
        }

        public bool Confirm(DatabaseContext db, int id)
        {
            return db.Users.Where(u => u.Id == id).Update(u => new User() { ConfirmedAt = DateTime.UtcNow }) > 0;
        }

        public bool UpdatePassword(DatabaseContext db, int id, string newPassword)
        {
            var hashedPassword = new PasswordHasher<User>().HashPassword(null, newPassword);
            return db.Users.Where(u => u.Id == id).Update(u => new User() { PasswordHash = hashedPassword }) > 0;
        }

        public bool IsValidPassword(string hashedPassword, string password)
        {
            return new PasswordHasher<User>().VerifyHashedPassword(null, hashedPassword, password) != PasswordVerificationResult.Failed;
        }

        public bool ExistAny(DatabaseContext db, string login)
        {
            return db.Users.Any(u => u.Login == login);
        }

        public bool ExistAny(DatabaseContext db, string login, out int userId)
        {
            userId = db.Users.Where(u => u.Login == login).Select(u => u.Id).FirstOrDefault();
            return userId > 0;
        }

        public bool ExistAny(DatabaseContext db, int id, string password)
        {
            var hashedPassword = db.Users.Where(u => u.Id == id).Select(u => u.PasswordHash).FirstOrDefault();
            return hashedPassword?.Length > 0 && IsValidPassword(hashedPassword, password);
        }

        public bool ExistAny(DatabaseContext db, int id)
        {
            return db.Users.Any(u => u.Id == id);
        }

        public string ReadLogin(DatabaseContext db, int id)
        {
            var login = db.Users.Where(u => u.Id == id).Select(u => u.Login).FirstOrDefault();

            if (string.IsNullOrEmpty(login))
                throw new HttpNotFoundException(nameof(User), id);

            return login;
        }

        public User ReadonlyOrNull(DatabaseContext db, int id, string checkPassword = null)
        {
            var user = db.Users.AsNoTracking().Where(u => u.Id == id).OrderBy(u => u.Id).FirstOrDefault();
            if (user != null && checkPassword?.Length > 0 && new PasswordHasher<User>().VerifyHashedPassword(null, user.PasswordHash, checkPassword) == PasswordVerificationResult.Failed)
            {
                return null;
            }
            return user;
        }

        public User ReadonlyOrNull(DatabaseContext db, string login, string checkPassword = null)
        {
            var user = db.Users.AsNoTracking().Where(u => u.Login == login).OrderBy(u => u.Login).FirstOrDefault();
            if (user != null && checkPassword?.Length > 0 && new PasswordHasher<User>().VerifyHashedPassword(null, user.PasswordHash, checkPassword) == PasswordVerificationResult.Failed)
            {
                return null;
            }
            return user;
        }

        public User Readonly(DatabaseContext db, int id)
        {
            var user = ReadonlyOrNull(db, id);

            if (user == null)
                throw new HttpNotFoundException(nameof(User), id);

            return user;
        }
    }
}
