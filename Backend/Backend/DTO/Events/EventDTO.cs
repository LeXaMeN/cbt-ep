﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Events
{
    public class EventDTO
    {
        [Range(0, int.MaxValue)]
        public int MembersLimit { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [StringLength(4096)]
        public string Description { get; set; }

        [Required]
        [StringLength(128)]
        public string Time { get; set; }
        [Required]
        [StringLength(256)]
        public string Location { get; set; }
    }
}
