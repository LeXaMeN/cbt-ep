﻿using Backend.Data.Context;
using Backend.Data.Repositories;
using Backend.DTO.UserTokens;
using Backend.Exceptions;
using Backend.Providers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserTokenService
    {
        public UserRepository UserRepository { get; }
        public UserTokenRepository UserTokenRepository { get; }

        public IdentityTokenProvider IdentityTokenProvider { get; }

        public UserTokenService(UserRepository userRepository, UserTokenRepository userTokenRepository, IdentityTokenProvider identityTokenProvider)
        {
            UserRepository = userRepository;
            UserTokenRepository = userTokenRepository;
            IdentityTokenProvider = identityTokenProvider;
        }

        public IdentityToken Create(DatabaseContext db, UserTokenDTO dto, string remoteIpAddress)
        {
            var user = UserRepository.ReadonlyOrNull(db, dto.Login, dto.Password);
            if (user == null || user.ConfirmedAt == null)
                throw new HttpBadRequestException("Invalid login or password.");

            return IdentityTokenProvider.Create(db, user.Id, user.Login, new[] { "user" }, remoteIpAddress);
        }
    }
}
