﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    public class HttpFoundException : Exception, IHttpException
    {
        public int StatusCode => (int)HttpStatusCode.Found;
        public string DefaultMessage => "Object already exists.";

        public HttpFoundException(string message) : base(message) { }
        public HttpFoundException(string message, Exception innerException) : base(message, innerException) { }

        public HttpFoundException(string entityName, object entityKey, Exception innerException = null)
            : base($"{entityName} #{entityKey ?? "NULL"} already exists.", innerException) { }
    }
}
