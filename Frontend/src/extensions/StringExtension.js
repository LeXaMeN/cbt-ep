String.isWhitespace = function isWhitespace(char) {
    return ' \t\n\r\f'.indexOf(char) !== -1;
};

String.prototype.trimStart = function (char = null) {
    let start = 0;

    for (let i = start; i < this.length; i++)
        if (char ? (this.charAt(i) === char) : String.isWhitespace(this.charAt(i)))
            start++;
        else
            break;

    return this.substring(start, this.length);
};

String.prototype.trimEnd = function (char = null) {
    let end = this.length;

    for (let i = end - 1; i >= 0; i--)
        if (char ? (this.charAt(i) === char) : String.isWhitespace(this.charAt(i)))
            end--;
        else
            break;

    return this.substring(0, end);
};

// String.prototype.trim = function (char = null) {
//     return this.trimStart(char).trimEnd(char);
// };

/**
 * Camel case to title
 * https://stackoverflow.com/questions/7225407
 *
 * @return {string}
 */
String.prototype.camelToTitle = function () {
    return this
        .replace(/^./, (match) => match.toUpperCase())
        .replace(/([A-Z]+)/g, " $1")
        .replace(/([A-Z][a-z])/g, " $1")
        .trimStart();
};
/**
 * https://stackoverflow.com/a/54346501
 *
 * @return {string}
 */
String.prototype.decodeHtmlCharCodes = function () {
    return this.replace(/(&#(\d+);)/g, function (match, capture, charCode) {
        return String.fromCharCode(charCode);
    });
};
