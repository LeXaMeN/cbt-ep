﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Data;

namespace Backend.Extensions.Database
{
    public static class DatabaseFacadeExtension
    {
        public static void Transaction(this DatabaseFacade databaseFacade, Action action)
        {
            var dbTransaction = databaseFacade.CurrentTransaction != null ? null : databaseFacade.BeginTransaction(IsolationLevel.ReadCommitted);

            try
            {
                action.Invoke();

                dbTransaction?.Commit();
            }
            catch
            {
                dbTransaction?.Rollback();
                throw;
            }
            finally
            {
                dbTransaction?.Dispose();
            }
        }

        public static TResult Transaction<TResult>(this DatabaseFacade databaseFacade, Func<TResult> func)
        {
            var dbTransaction = databaseFacade.CurrentTransaction != null ? null : databaseFacade.BeginTransaction(IsolationLevel.ReadCommitted);

            try
            {
                var value = func.Invoke();

                dbTransaction?.Commit();

                return value;
            }
            catch
            {
                dbTransaction?.Rollback();
                throw;
            }
            finally
            {
                dbTransaction?.Dispose();
            }
        }
    }
}
