﻿using Backend.Data.Context;
using Backend.Data.Repositories;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("events/{eventId}/members")]
    public class EventMemberController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private EventMemberService EventMemberService { get; }

        public EventMemberController(DatabaseContext database, EventMemberService eventMemberService)
        {
            Database = database;
            EventMemberService = eventMemberService;
        }

        [HttpPost]
        public IActionResult Post([FromRoute] int eventId)
        {
            try
            {
                var result = EventMemberService.Create(Database, User.GetId(), eventId);
                return Ok(result);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Post), new { UserId = User.GetId(), EventId = eventId });
            }
        }

        [HttpGet]
        public IActionResult GetList([FromRoute] int eventId)
        {
            try
            {
                var members = EventMemberService.ReadonlyList(Database, User.GetId(), eventId);
                return Ok(members);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(GetList), new { UserId = User.GetId(), EventId = eventId });
            }
        }

        [HttpDelete]
        public IActionResult Delete([FromRoute] int eventId)
        {
            try
            {
                var result = EventMemberService.EventMemberRepository.Delete(Database, User.GetId(), eventId);
                return Ok(result);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Delete), new { UserId = User.GetId(), EventId = eventId });
            }
        }
    }
}
