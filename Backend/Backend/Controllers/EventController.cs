﻿using Backend.Data.Context;
using Backend.Data.Repositories;
using Backend.DTO.Events;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]s")]
    public class EventController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private EventService EventService { get; }

        public EventController(DatabaseContext database, EventService eventService)
        {
            Database = database;
            EventService = eventService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] EventDTO dto)
        {
            try
            {
                var @event = EventService.EventRepository.Create(Database, User.GetId(), dto.MembersLimit, dto.Name, dto.Description, dto.Time, dto.Location);
                return Ok(@event);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Post), new { UserId = User.GetId() }, dto);
            }
        }

        [HttpPut("{eventId}")]
        public IActionResult Put([FromRoute] int eventId, [FromBody] EventDTO dto)
        {
            try
            {
                var @event = EventService.EventRepository.Update(Database, eventId, User.GetId(), dto.MembersLimit, dto.Name, dto.Description, dto.Time, dto.Location);
                return Ok(@event);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Put), new { EventId = eventId, UserId = User.GetId() }, dto);
            }
        }

        [HttpGet("{eventId}")]
        public IActionResult Get([FromRoute] int eventId)
        {
            try
            {
                var @event = EventService.EventRepository.Readonly(Database, eventId);
                return Ok(@event);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Get), new { EventId = eventId, UserId = User.GetId() });
            }
        }

        [HttpGet("my")]
        public IActionResult GetMyList()
        {
            try
            {
                var events = EventService.EventRepository.ReadonlyList(Database, User.GetId());
                return Ok(events);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(GetMyList), new { UserId = User.GetId() });
            }
        }

        [HttpGet]
        public IActionResult GetList()
        {
            try
            {
                var events = EventService.EventRepository.ReadonlyList(Database);
                return Ok(events);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(GetList), new { UserId = User.GetId() });
            }
        }

        [HttpDelete("eventId")]
        public IActionResult Delete(int eventId)
        {
            try
            {
                var result = EventService.Delete(Database, User.GetId(), eventId);
                return Ok(result);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Delete), new { UserId = User.GetId() });
            }
        }
    }
}
