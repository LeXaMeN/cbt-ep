﻿using Backend.Data.Repositories;
using Backend.Extensions.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Services.Background
{
    public class UserTokenBackgroundService : BackgroundService
    {
        private IConfiguration Configuration { get; }
        private ILogger Logger { get; }
        private IServiceProvider ServiceProvider { get; }

        private UserTokenRepository UserTokenRepository { get; }

        public UserTokenBackgroundService(IConfiguration configuration, ILogger<UserTokenBackgroundService> logger, IServiceProvider serviceProvider, UserTokenRepository userTokenRepository)
        {
            Configuration = configuration;
            Logger = logger;
            ServiceProvider = serviceProvider;
            UserTokenRepository = userTokenRepository;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (stoppingToken.IsCancellationRequested == false)
            {
                var waitMilliseconds = 2 * 60 * 60 * 1000; // 2 hours

                try
                {
                    using (var scope = ServiceProvider.CreateScope(out var db))
                    {
                        var deleted = UserTokenRepository.DeleteAllExpired(db);
                        if (deleted > 0)
                            Logger.LogInformation($"UserTokenBackgroundService delete {deleted} expired tokens.");
                    }
                }
                catch (Exception exception)
                {
                    Logger.LogCritical(exception, $"{nameof(UserTokenBackgroundService)}.{nameof(ExecuteAsync)}");
                    waitMilliseconds = 5 * 60 * 1000; // 5 minutes
                }
                await Task.Delay(waitMilliseconds);
            }
        }
    }
}
