import User from '../entity/User';

export default class UserProfile {
    constructor(entity = null) {
        if (entity) {
            let value = Object.assign(new UserProfile(), entity);
            if (value.user) {
                value.user = new User(value.user);
            }
            return value;
        }
    }

    /**
     * @type number
     */
    userId;
    /**
     * @type ?User
     */
    user;
    /**
     * @type string
     */
    name;
    /**
     * @type string
     */
    surname;
    /**
     * @type number
     */
    age;
}
