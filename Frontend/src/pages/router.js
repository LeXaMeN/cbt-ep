import Router from 'vue-router';

// Pages
import home from '../pages/home.vue';
import login from '../pages/login.vue';
import profile from '../pages/profile.vue';
import events from '../pages/events.vue';

// Errors
import error404 from '../components/error/404.vue';

/**
 * Routes
 */
export default new Router({
    mode: 'history',
    routes: [
        // main
        {path: '/', name: 'home', component: home, meta: {title: 'Event planning'}},
        {path: '/login', name: 'login', component: login, meta: {title: 'Log In'}},
        {path: '/profile', name: 'profile', component: profile, meta: {title: 'Profile'}},
        {path: '/events', name: 'events', component: events, meta: {title: 'Events'}},
        // error
        {path: '*', name: 'error-404', component: error404, meta: {title: 'Error 404 - Not found'}}
    ]
});
