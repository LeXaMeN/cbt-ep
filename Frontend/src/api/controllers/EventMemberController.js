import {Api} from '../Api';
import EventMemberDTO from '$api/dto/events/EventMemberDTO';

export default class EventMemberController {
    /**
     * @param {number} eventId
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Post(eventId, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'post',
            url: '/events/' + eventId + '/members'
        }, thenFunction, catchFunction, finallyFunction);
    }

    /**
     * @param {number} eventId
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static GetList(eventId, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'get',
            url: '/events/' + eventId + '/members'
        }, (list) => thenFunction(Api.ListToEntities(list, EventMemberDTO)), catchFunction, finallyFunction);
    }

    /**
     * @param {number} eventId
     * @param {function} thenFunction
     * @param {?function} catchFunction
     * @param {?function} finallyFunction
     */
    static Delete(eventId, thenFunction, catchFunction = null, finallyFunction = null) {
        Api.Request({
            method: 'delete',
            url: '/events/' + eventId + '/members'
        }, thenFunction, catchFunction, finallyFunction);
    }
}
