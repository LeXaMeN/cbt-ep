export default class PaginatedCollectionDTO {
    /**
     * @type {number[]}
     * @private
     */
    _pages = [1];

    /**
     * @param {object?} object
     * @param {function?} entity
     */
    constructor(object = null, entity = null) {
        if (object) {
            let collection = Object.assign(new PaginatedCollectionDTO(), object);
            if (collection.list?.length && typeof entity === 'function') {
                collection.list.forEach((value, index) => {
                    collection.list[index] = new entity(value);
                });
            }
            return collection;
        }
    }

    /**
     * @type number
     */
    page = 1;
    /**
     * @type number
     */
    pages = 1;
    /**
     * @type number
     */
    limit = 25;
    /**
     * @type number
     */
    count = 1;
    /**
     * @type object[]
     */
    list = [];
}
