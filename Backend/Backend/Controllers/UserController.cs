﻿using Backend.Data.Context;
using Backend.Data.Repositories;
using Backend.DTO.Users;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("[controller]s")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IConfiguration Configuration { get; }
        private DatabaseContext Database { get; }
        private UserService UserService { get; }

        public UserController(IConfiguration configuration, DatabaseContext database, UserService userService)
        {
            Configuration = configuration;
            Database = database;
            UserService = userService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserRegistrationDTO dto)
        {
            try
            {
                UserService.Create(Database, dto, HttpContext.Connection.RemoteIpAddress.ToString());
                return Ok();
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Post), new { Login = dto.Login });
            }
        }

        [HttpGet("{userId}/confirm")]
        public IActionResult GetConfirmation([FromRoute] int userId, [FromQuery] string token)
        {
            try
            {
                UserService.Confirm(Database, userId, token);
                return Redirect(Configuration["Host:Links:Profile"]);
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(GetConfirmation), new { UserId = userId, Token = token });
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            try
            {
                return Ok(UserService.UserRepository.Readonly(Database, User.GetId()));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Get), new { UserId = User.GetId() });
            }
        }
    }
}
