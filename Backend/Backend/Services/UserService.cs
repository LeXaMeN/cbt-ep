﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Data.Repositories;
using Backend.DTO.Users;
using Backend.Exceptions;
using Backend.Providers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserService
    {
        private IConfiguration Configuration { get; }

        public UserRepository UserRepository { get; }
        public UserTokenRepository UserTokenRepository { get; }

        private EmailProvider EmailProvider { get; }

        public UserService(IConfiguration configuration, UserRepository userRepository, UserTokenRepository userTokenRepository, EmailProvider emailProvider)
        {
            Configuration = configuration;
            UserRepository = userRepository;
            UserTokenRepository = userTokenRepository;
            EmailProvider = emailProvider;
        }

        public void Create(DatabaseContext db, UserRegistrationDTO dto, string remoteIpAddress)
        {
            var user = UserRepository.ReadonlyOrNull(db, dto.Login);
            if (user == null)
            {
                user = UserRepository.Create(db, dto.Login, dto.Password);
            }
            else
            {
                if (user.ConfirmedAt != null)
                    throw new HttpBadRequestException($"Email `{dto.Login}` already taken.");
            }

            var registrationToken = UserTokenRepository.Create(db, user.Id, UserTokenType.Registration, remoteIpAddress, expiresAt: DateTime.UtcNow.AddDays(1));
            var confirmUrl = $"{Configuration["Api:Url"]}/users/{user.Id}/confirm?token={registrationToken.Value}";
            EmailProvider.Send("Registration сonfirmation", confirmUrl, user.Login);
        }

        public void Confirm(DatabaseContext db, int userId, string tokenValue)
        {
            var token = UserTokenRepository.ReadonlyOrNull(db, userId, UserTokenType.Registration, tokenValue, checkExpires: true);
            if (token == null)
            {
                throw new HttpNotFoundException("Token not found.");
            }
            UserRepository.Confirm(db, userId);
            UserTokenRepository.DeleteAll(db, userId, UserTokenType.Registration);
        }
    }
}
